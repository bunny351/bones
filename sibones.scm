;;;; run bones inside si


;(load "~/bones/megalet.scm")
;(load "~/bones/match.scm")
;(load "~/bones/pp.scm")
;(load "~/bones/support.scm")


(load "version.scm")
(load "alexpand.scm")
(load "source.scm")
(load "cp.scm")
(load "simplify.scm")
(load "uv.scm")
(load "cc.scm")
(load "cps.scm")
(load "mangle.scm")
(load "program.scm")
(load "tsort.scm")
(load "ra.scm")
(load "cmplr.scm")
(load "main.scm")
(load "x86_64.scm")
